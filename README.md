# Directed Word Graphs


Overview
--------
This repository consists of a basic class that is capable of converting text into a directed word graph that can then be used to auto-generate text passages using either random walks or markov chains.  It is fairly bare-bones and does not have fancy features.  There is also a test directory and a main demo file.


Background
----------
This was one of the Python projects I used to familiarize myself with the language, configure my environment/tools, and learn the `pytest` testing framework.  I have migrated it to this repo for demo purposes as well as to have for reference and possible future work.  It is not meant to be of practical use to anyone.

Future work can involve improvements/additions either to improve functionality or for insructive purposes.  As such, I might add useless or unrelated features if I want to learn something.


Future Work
-----------
- Add support for saving/loading DOT format
- Add support for saving/loading databases (SQL, NoSQL)
- More advanced parsing/generation
	- Add support for quoted text
	- Add support for reading plays/scripts (separate graphs per speaker)
	- Add support for different output format (essays, scripts, quotes, etc.)
