from random import randint

from DirectedWordGraph import DirectedWordGraph as WordGraph


# Maximum nodes to build a sentence from
MAX_LENGTH = 20

# Constraints on number of paragraphs and sentences
NUM_PARAGRAPHS = 5
MIN_SENTENCES = 5
MAX_SENTENCES = 10


def main():
    graph = WordGraph()

    graph.create_from_text_file("AliceInWonderland.txt")

    # graph.save_json("Alice.json", readable=True)
    # graph.load_json("Alice.json")

    for _ in range(NUM_PARAGRAPHS):
        nsent = randint(MIN_SENTENCES, MAX_SENTENCES)
        print(graph.markov_chain(nsent, max_len=MAX_LENGTH))
        print()


if __name__ == "__main__":
    main()
