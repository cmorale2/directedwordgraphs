import os, json

import pytest

from DirectedWordGraph import DirectedWordGraph as WordGraph


@pytest.fixture
def data_json():
    """Reads JSON data file containing test data"""
    filename = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                            "data_FromFile.json")
    with open(filename, "r") as file:
        return json.load(file)


def test_load_json(data_json):
    """Tests converting text to graph

    The input data is a list of files and graphs
    Each list element has the form:
        [filename, graph]
    """

    for row in data_json:
        graph = WordGraph()

        filename = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                row[0])

        graph.create_from_text_file(filename)

        assert graph._graph == row[1]
