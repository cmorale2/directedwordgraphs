import os, json

import pytest

from DirectedWordGraph import DirectedWordGraph as WordGraph


@pytest.fixture
def data_addpost():
    """Reads JSON data file containing test data"""
    filename = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                            "data_AddPost.json")
    with open(filename, "r") as file:
        return json.load(file)


def test_add_pp(data_addpost):
    """Tests adding connections and post-processing of graph

    The input data is organized as list of connections and results
    For each list element of the test data:
      'connections' --> List of connections, where each list element is
                        itself a list of nodes ([from, to])
      'post-process' -> If exists, boolean flag of whether final result is
                        post-processed (probabilities) or not
      'graph' --------> What final output should be.  Since representation in
                        class is dictionary, a simple comparison should be
                        enough to check this for validity
    """

    for row in data_addpost:
        graph = WordGraph()
        for conn in row['connections']:
            graph._add_connection(conn[0], conn[1])
        if "post-process" in row.keys() and row['post-process']:
            graph._post_process()
        assert graph._is_valid()
        assert graph._graph == row['graph']
