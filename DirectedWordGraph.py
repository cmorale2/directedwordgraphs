import json
import random



class DirectedWordGraph:
    """Directed graph implementation for words as nodes"""



    def __init__(self):
        """Initialize object.

        The graph is represented as a dictionary.
            Keys -> Nodes
            Vals -> Dictionary of connections:
                    Keys -> Nodes (keys of parent dict)
                    Vals -> [# of occurrences of connection,
                             Probability of connection (post-processed)]

        The boolean flag fpprocess indicates whether the graph has been
        post-processed or not.
        """
        self._graph = {}
        self._fpprocess = False



    # ----------------------------------------------------------------------- #
    #
    #
    #          Public functions for generating graph from text
    #
    #
    # ----------------------------------------------------------------------- #
    def create_from_text_file(self, filename):
        """Generate word graph from text in file."""
        if not isinstance(filename, str):
            raise TypeError("Expected string, got {}"
                            .format(type(filename).__name__))

        with open(filename, "r") as file:
            last_word = None
            for line in file:
                last_word = self._add_from_text(line, begin=last_word)

        self._post_process()


    def create_from_text(self, string):
        """Generate word graph from text in string."""
        if not isinstance(string, str):
            raise TypeError("Expected string, got {}"
                            .format(type(string).__name__))

        self._add_from_text(string)

        self._post_process()



    # ----------------------------------------------------------------------- #
    #
    #
    #        Public functions for generating text from graph
    #
    #
    # ----------------------------------------------------------------------- #
    def random_walk(self, num_sentences, max_len=None, end_punct="."):
        """Generate a section of text using a random walk."""
        if not isinstance(num_sentences, int):
            raise TypeError("Number of sentences must be int, was {}"
                            .format(type(num_sentences).__name__))
        if not (max_len is None or isinstance(max_len, int)):
            raise TypeError("Maximum length must be int, was {}"
                            .format(type(max_len).__name__))
        if not (end_punct is None or isinstance(end_punct, str)):
            raise TypeError("Ending punctuation must be string, was {}"
                            .format(type(end_punct).__name__))

        if num_sentences < 1:
            raise ValueError("Number of sentences must be >= 1, was {}"
                             .format(num_sentences))
        if max_len is not None and max_len < 1:
            raise ValueError("Max length must be >= 1, was {}"
                             .format(max_len))

        if not self._graph.keys():
            raise ValueError("The graph has not been created yet.")

        out_str = self._random_sentence('walk', max_len, end_punct)
        for _ in range(num_sentences-1):
            out_str += " " + self._random_sentence('walk', max_len, end_punct)

        return out_str


    def markov_chain(self, num_sentences, max_len=None, end_punct="."):
        """Generate a section of text using a markov chain."""
        if not isinstance(num_sentences, int):
            raise TypeError("Number of sentences must be int, was {}"
                            .format(type(num_sentences).__name__))
        if not (max_len is None or isinstance(max_len, int)):
            raise TypeError("Maximum length must be int, was {}"
                            .format(type(max_len).__name__))
        if not (end_punct is None or isinstance(end_punct, str)):
            raise TypeError("Ending punctuation must be string, was {}"
                            .format(type(end_punct).__name__))

        if num_sentences < 1:
            raise ValueError("Number of sentences must be >= 1, was {}"
                             .format(num_sentences))
        if max_len is not None and max_len < 1:
            raise ValueError("Max length must be >= 1, was {}"
                             .format(max_len))

        if not self._graph.keys():
            raise ValueError("The graph has not been created yet.")

        out_str = self._random_sentence('markov', max_len, end_punct)
        for _ in range(num_sentences-1):
            out_str += " "
            out_str += self._random_sentence('markov', max_len, end_punct)

        return out_str



    # ----------------------------------------------------------------------- #
    #
    #
    #       Public functions for saving and loading graph from file
    #
    #
    # ----------------------------------------------------------------------- #
    def save_json(self, filename, readable=False):
        """Save word graph to json file."""
        if not isinstance(filename, str):
            raise TypeError("Filename must be string, was {}"
                            .format(type(filename).__name__))
        if not isinstance(readable, bool):
            raise TypeError("Readable flag must be bool, was {}"
                            .format(type(readable).__name__))

        self._post_process()

        with open(filename, "w") as file:
            if readable:
                json.dump(self._graph, file, indent="\t")
            else:
                json.dump(self._graph, file)


    def load_json(self, filename):
        """Load word graph from json file."""
        if not isinstance(filename, str):
            raise TypeError("Filename must be string, was {}"
                            .format(type(filename).__name__))

        with open(filename, "r") as file:
            self._graph = json.load(file)

        if not self._is_valid():
            self._graph = {}
            self._fpprocess = False
            raise ValueError("File ({}) does not contain a valid graph"
                             .format(filename))

        self._post_process()




    # ----------------------------------------------------------------------- #
    #
    #
    #                       Private functions
    #
    #
    # ----------------------------------------------------------------------- #
    def _debug_dump(self):
        """Dump the graph for debugging."""
        print("GRAPH DUMP:")
        for key, val in self._graph.items():
            print("{}:".format(key))
            for conn, cnts in val.items():
                print("\t{}: {:3d}, {:01.03f}".format(conn, cnts[0], cnts[1]))
        print()


    def _is_valid(self):
        """Check if graph in object is valid or not."""
        for val in self._graph.values():
            for conn, cnts in val.items():
                if conn not in self._graph.keys():
                    return False
                if not isinstance(cnts[0], int) or cnts[0] < 1:
                    return False
                if not isinstance(cnts[1], (int, float)) \
                   or cnts[1] < 0 or cnts[1] > 1:
                    return False
        return True


    def _add_connection(self, nfrom, nto):
        """Add a directed connection to graph.

        If either of the nodes do not exist, they will be created.  If a
        connection already exists, its count will be incremented, otherwise
        it will be set to 1 by default.
        """
        self._fpprocess = False

        if nto not in self._graph.keys():
            self._graph[nto] = {}

        if nfrom not in self._graph.keys():
            self._graph[nfrom] = {}

        if nto not in self._graph[nfrom].keys():
            self._graph[nfrom][nto] = [1, 0]
        else:
            self._graph[nfrom][nto][0] += 1


    def _post_process(self):
        """Calculate probabilities of each connection in graph.

        Goes through nodes of graph and calculates the probability of each
        connection given the total number of connections and their counts.

        This process involves looping through all nodes and connections, so
        this function should only be called after all desired connections have
        been added to the graph.
        """
        if self._fpprocess:
            return

        for val in self._graph.values():
            # Get sum of connection occurrences (count all connections)
            num_sum = sum(cnt[0] for cnt in val.values())
            # Calculate and assign probabilities
            for cnts in val.values():
                cnts[1] = cnts[0] / num_sum

        self._fpprocess = True


    def _add_from_text(self, string, begin=None):
        """Update directed graph of words/punctuation from string.

        Can specify the previous word with 'begin' to ensure connection will
        exist between text segments

        All words retain their case.
        Hyphenated words count as a single node
        A word with a single quote inside them are a single node (he's)
        A word beginning with punctuation is a single node ($20)
        Punctuation marks will be their own node.
            - Double hyphen (--) counts as one node
            - Double endings count as a single node (!?)
            - Ellipsis counts as one node (...)
        Some punctuation is dropped completely
            - Quotes and double quotes (' " `)
            - Parenthesis and brackets

        Returns last node from string for continuity (next call's 'begin')
        """
        assert isinstance(string, str)
        assert begin is None or isinstance(begin, str)

        self._fpprocess = False

        words = string.split()

        if begin is not None:
            words.insert(0, begin)

        for i, word in enumerate(words):
            # Ignore punctuation we don't care about
            while word[0] in "'`\"()[]}{":
                word = word[1:]
            while word[-1] in "'`\"()[]}{":
                word = word[:-1]
            words[i] = word
            # Catch double-hyphen that's connected to neighboring words
            if len(word) > 3 \
              and word[:2] != '--' and word[-2:] != '--' \
              and "--" in word:
                dash_words = word.split('--')
                word = dash_words[0]
                words[i] = dash_words.pop(0)
                ind_cnt = 1
                while dash_words:
                    words.insert(i+ind_cnt, '--')
                    words.insert(i+ind_cnt+1, dash_words.pop(0))
                    ind_cnt += 2
                continue
            # If no ending punctuation, don't need to do anything
            if word[-1].isalnum():
                continue
            # Rip punctuation into separate variable
            punct = ""
            while word and not word[-1].isalnum():
                punct = word[-1] + punct
                word = word[:-1]
            # Only update list if the punctuation wasn't the entire word
            # otherwise we'll loop forever
            if word:
                words[i] = word
                words.insert(i+1, punct)

        for i in range(len(words)-1):
            self._add_connection(words[i], words[i+1])

        return words[-1]


    def _random_sentence(self, kind, max_len, end_punct):
        """Generate a sentence randomly from word graph.

        The kind must be either 'walk' or 'markov'
            walk   -> random walk, all paths equally likely
            markov -> markov chain, randomly select path based on probability

        The maximum length is the maximum number of nodes allowed in the
        sentence.  If this happens and the final node is not a sentence-ending
        punctuation mark, one will be added.  The one that will be added is
        specified in the arguments.  If set to None, none will be added.
        """
        assert kind in ["walk", "markov"]
        assert isinstance(max_len, int)
        assert end_punct is None or isinstance(end_punct, str)

        # Find an initial word that is not punctuation and that is capitalized
        cur_word = "."
        while not cur_word[-1].isalnum() or not cur_word[0].isupper():
            cur_word = random.choice(list(self._graph.keys()))
        words = [cur_word]

        # Add words to list until max_len is reached OR we encounter an ending
        # punctuation OR there are no nodes to go to
        while True:
            # If a maximum length is set and we've hit it, break
            if max_len is not None and len(words) >= max_len:
                break
            # If there are no nodes to go to next, end the sentence.
            if not self._graph[words[-1]].values():
                break
            # If we hit a sentence-ending punctuation, end the sentence
            if words[-1][-1] in ".!?":
                break
            # Pick next word randomly based on 'kind'
            if kind == "walk":
                next_word = random.choice(list(self._graph[words[-1]].keys()))
            else: # kind == "markov"
                keys = []
                probs = []
                for key, val in self._graph[words[-1]].items():
                    keys.append(key)
                    probs.append(val[1])
                next_word = random.choices(keys, weights=probs)[0]
            words.append(next_word)

        # We need to make sure the sentence ended gracefully.  There are two
        # situations that require adjustment:
        #   -> The last word is an actual word
        #   -> The last word is non-sentence-ending punctuation
        if words[-1][-1].isalnum():
            words.append(end_punct)
        elif words[-1][-1] not in ".!?" and end_punct is not None:
            words[-1] = end_punct

        # Combine word list into string, adding spaces where needed
        out_str = words.pop(0)
        while words:
            if words[0][-1].isalnum() or words[0][-1] in "-":
                out_str += " " + words.pop(0)
            else:
                out_str += words.pop(0)

        return out_str
